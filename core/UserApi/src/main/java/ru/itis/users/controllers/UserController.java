package ru.itis.users.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.users.entities.User;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @GetMapping()
    public List<User> getAllBooks() {

        User user1 = User.builder()
                .username("john_doe")
                .password("password123")
                .direction("north")
                .build();

        User user2 = User.builder()
                .username("jane_smith")
                .password("password456")
                .direction("south")
                .build();

        User user3 = User.builder()
                .username("alice_jones")
                .password("password789")
                .direction("east")
                .build();

        User user4 = User.builder()
                .username("bob_brown")
                .password("password321")
                .direction("west")
                .build();

        User user5 = User.builder()
                .username("charlie_black")
                .password("password654")
                .direction("northeast")
                .build();

        User user6 = User.builder()
                .username("dave_white")
                .password("password987")
                .direction("northwest")
                .build();

        User user7 = User.builder()
                .username("eve_green")
                .password("password159")
                .direction("southeast")
                .build();

        User user8 = User.builder()
                .username("frank_blue")
                .password("password753")
                .direction("southwest")
                .build();

        User user9 = User.builder()
                .username("grace_gray")
                .password("password951")
                .direction("north-northwest")
                .build();

        User user10 = User.builder()
                .username("henry_yellow")
                .password("password357")
                .direction("south-southwest")
                .build();

        return  List.of(user1, user2, user3, user4, user5, user6, user7, user8, user9, user10);
    }
}
